#Irish Triads Alexa Skill
A simple [AWS Lambda](http://aws.amazon.com/lambda) function that demonstrates how to write a skill for the Amazon Echo using the Alexa SDK.

##Description
This is a simple skill that provides a random "irish triad" from a pre-defined list of triads.

##History Lesson
The Irish Triads were a collection of bits of Irish wisdom, generally in sets of three.  Example: Three things whereby the devil shows himself in man: by his face, by his gait, by his speech.

These were originally translated by Kuno Meyer and are available on the CELT website here: http://www.ucc.ie/celt/online/T103006.html