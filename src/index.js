var APP_ID = undefined; //OPTIONAL: replace with "amzn1.echo-sdk-ams.app.[your-unique-value-here]";

/**
 * Array containing triads
 */
var FACTS = [

"Four elements of folly: silliness, bias, wrangling, foulmouthedness.",
"Four elements of wisdom: patience, docility, sobriety, well-spokenness; for every patient person is wise, and every docile person is a sage, every sober person is generous, every well-spoken person is tractable.",
"Four hatreds of a chief: a silly flighty man, a slavish useless man, a lying dishonourable man, a talkative man who has no story to tell.",
"Four on whom there is neither restraint nor rule: the servant of a priest, a miller’s hound, a widow’s son, and a stripper’s calf.",
"The four deaths of judgment: to give it in falsehood, to give it without forfeiture, to give it without precedent, to give it without knowledge.",
"The three chief sins: avarice, gluttony, lust.",
"The three comfortable places of Ireland: the abbotship of Lusk, the kingship of the three Cualu, the vice-abbotship of Armagh.",
"The three dark places of Ireland: the cave of Knowth, the cave of Slaney, the cave of Ferns.",
"The three desert places of Ireland: Fid Mór (Great Wood) in Coolney, Fid Déicsen (Spy-wood) in Tuirtri, the Wood of Moher in Connaught.",
"The three fairs of Ireland: the fair of Teltown, the fair of Croghan, the fair of Colman Elo.",
"The three familiar places of Ireland: Tralee, Logher, the Fews.",
"The three fields (?) of Ireland: the land of Rathlynan, Slieve Comman, Slieve Manchain.",
"The three fords of Ireland: Ath Cliath (Hurdle-ford), Athlone (the Ford of Luan), Ath Caille (Wood-ford).",
"The three forts of Ireland: Dunseverick, Dun Cermna, Cathir Conree.",
"The three halidoms of the men of Ireland: breast, cheek, knee.",
"The three heights of Ireland: Croagh Patrick, Ae Chualann.",
"The three highroads of Ireland: Slige Dala.",
"The three households of Ireland: the household of Tara, the household of Cashel, the household of Croghan",
"The three lakes of Ireland: Lough Neagh, Lough Ree, Lough Erne.",
"The three meadows of Ireland: Clonmacnois, Clones, Clonard.",
"The three mountain-passes of Ireland: Baltinglass, the Pass of Limerick, the Pass of Dublin.",
"The three mountains of Ireland: Slieve Gua.",
"The three places of Ireland to alight at: Derry, Taghmon, Kilmainham.",
"The three plains of Ireland: Moy Bray, Moy Croghan, Moy Liffey.",
"The three plains of Ireland: the plain of Meath, Moylinny, Moy-Liffey.",
"The three rent-paying places of Ireland: Clonard, Glendalough, Louth.",
"The three ridges of Ireland: Druim Fingin, Druim nDrobcoil, Druim Leithe.",
"The three rivers of Ireland: the Shannon, the Boyne, the Bann.",
"The three stone-buildings of Ireland: Armagh, Clonmacnois, Kildare.",
"The three strands of Ireland: the strand of Ross Airgit.",
"The three uneven places of Ireland: Breffny, the Burren, Beare.",
"The three unlucky places of Ireland: the abbotship of Bangor, the abbotship of Lynally, the kingship of Mugdorn Maigen.",
"The three waterfalls of Ireland: Assaroe, Eas Danainne, Eas Maige.",
"The three wells of Ireland: the Well of the Desi, the Well of Uarbel, the Well of Uaran Garaid.",
"The three worst welcomes: a handicraft in the same house with the inmates, scalding water upon the feet, salt food without a drink.",
"Three accomplishments of Ireland: a witty stave, a tune on the harp, shaving a face.",
"Three after-sorrows: a wooer’s, a thief’s, a tale-bearer’s.",
"Three aged sisters: groaning, chastity, ugliness.",
"Three angry sisters: blasphemy, strife, foulmouthedness.",
"Three black husbandries: thatching with stolen things, putting up a fence with a proclamation of trespass, kiln-drying with scorching.",
"Three bloodsheds that need not be impugned: the bloodshed of battle, of jealousy, of mediating.",
"Three brutes whose trespasses count as human crimes: a chained hound, a ferocious ram, a biting horse.",
"Three brutish things that atone for crimes: a leashed hound, a spike in a wood, a lath . . . ",
"Three caldrons that are in every fort: the caldron of running (?), the caldron ‘goriath’, the caldron of guests.",
"Three candles that illumine every darkness: truth, nature, knowledge.",
"Three causes that do not die with neglect: the causes of an imbecile, and of oppression, and of ignorance.",
"Three chains by which evil propensity is bound: a covenant, a (monastic) rule, law.",
"Three characteristics of concupiscence: sighing, playfulness, visiting.",
"Three characteristics of obstinacy: long visits, staring, constant questioning.",
"Three coffers whose depth is not known: the coffer of a chieftain, of the Church, of a privileged poet.",
"Three cohabitations that do not pay a marriage-portion: taking her by force, outraging her without her knowledge through drunkenness, her being violated by a king.",
"Three cold things that seethe: a well, the sea, new ale.",
"Three concealments upon which forfeiture does not close: a wife’s dowry, the food of a married couple, a boy’s foster-fee.",
"Three contracts that are reversed by the decision of a judge: the contracts of a woman, of a son, of a cottar.",
"Three dark things of the world: giving a thing into keeping, guaranteeing, fostering.",
"Three darknesses into which women should not go: the darkness of mist, the darkness of night, the darkness of a wood.",
"Three dead ones that are paid for with living things: an apple-tree, a hazel-bush, a sacred grove.",
"Three dead things that give evidence on live things: a pair of scales, a bushel, a measuring-rod.",
"Three deaf ones of the world: warning to a doomed man, mocking a beggar, keeping a loose woman from lust.",
"Three deaths that are better than life: the death of a salmon, the death of a fat pig, the death of a robber.",
"Three debts which must not be neglected: debts of land, payment of a field, instruction (?) of poetry.",
"Three deposits that need not be returned: the deposits of an imbecile.",
"Three deposits with usufruct: depositing a woman, a horse, salt.",
"Three doors of falsehood: an angry pleading, a shifting foundation of knowledge, giving information without memory.",
"Three doors through which truth is recognized: a patient answer, a firm pleading, appealing to witnesses.",
"Three drops of a wedded woman: a drop of blood, a tear-drop, a drop of sweat.",
"Three duties of guarantorship: staying (at home), honesty, suffering (?); staying in one’s residence, honesty lest he utter falsehood, suffering (?) payment, viz. letting oneself be stripped for an illegal action instead of the debtor.",
"Three entertainers of a gathering: a jester, a juggler, a lap-dog.",
"Three excellences of dress: elegance, comfort, lastingness.",
"Three excellent things for a householder: proposing to a good woman, serving a good chief, exchanging for good land.",
"Three fair things that hide ugliness: good manners in the ill-favoured, skill in a serf, wisdom in the misshapen.",
"Three false sisters: ‘perhaps’, ‘may be’, ‘I dare say’.",
"Three fewnesses that are better than plenty: a fewness of fine words, a fewness of cows in grass, a fewness of friends around ale.",
"Three forbidden things of a church: a nun as a bellringer, a veteran in the abbotship, a drop upon the altar.",
"Three free ones that make slaves of themselves: a lord who sells his land, a queen who goes to a boor, a poet’s son who abandons his (father’s) craft.",
"Three glories of a gathering: a beautiful wife, a good horse, a swift hound.",
"Three glories of a gathering: a judge without perturbation, a decision without reviling, terms (agreed upon) without fraud.",
"Three glories of speech: steadiness, wisdom, brevity.",
"Three hands that are best in the world: the hand of a good carpenter, the hand of a skilled woman, the hand of a good smith.",
"Three hard things  Seven prohibitions: to go security for an outlaw, for a jester and for a madman, for a person without bonds, for an unfilial person, for an imbecile, for one excommunicated. Troublesome moreover is every security, for it is necessary for it to give sudden notice as regards every pledge which he gives, now beforehand, now afterwards.",
"Three hateful things in speech: stiffness, obscurity, a bad delivery.",
"Three holidays: visiting in the house of a blacksmith, visiting in the house of a carpenter, buying without bonds.",
"Three idiots that are in a bad guest-house: the chronic cough of an old hag, a brainless tartar of a girl, a hobgoblin of a gillie.",
"Three ill-bred sisters: fierceness, lustfulness, obduracy.",
"Three illnesses that are better than health: the lying-in of a woman with a male child, the fever of an abdominal disease that clears the bowels, a feverish passion to check evil by its good (?).",
"Three impossible demands: go! though you cannot go, bring what you have not got, do what you cannot do.",
"Three indications of dignity in a person: a fine figure, a free bearing, eloquence.",
"Three inheritances that are divided in the presence of heirs: the inheritance of a jester, of a madman, and of an old man.",
"Three irreverent sisters: importunity, frivolity, flightiness.",
"Three keys that unlock thoughts: drunkenness, trustfulness, love.",
"Three laughing-stocks of the world: an angry man, a jealous man, a niggard.",
"Three lawful handbreadths: a handbreadth between shoes and hose, a handbreadth between ear and hair, a handbreadth between the fringe of the tunic and the knee.",
"Three live ones that put away dead things: a deer shedding its horn, a wood shedding its leaves, cattle shedding their coat.",
"Three locks that lock up secrets: shame, silence, closeness.",
"Three maidens that bring hatred upon misfortune: talking, laziness, insincerity.",
"Three maidens that bring love to good fortune: silence, diligence, sincerity.",
"Three man-days: Thursday, Friday, Sunday. If women go to men on those days, they will not be loved, and their husbands will survive them. Saturday, however, is a common day. It is equally lucky to them. Monday is a free day to undertake any business.",
"Three nurses of dignity: a fine figure, a good memory, piety.",
"Three nurses of high spirits: pride, wooing, drunkenness.",
"Three nurses of theft: a wood, a cloak, night.",
"Three oaths that do not require fulfilment: the oath of a woman in birth-pangs, the oath of a dead man, the oath of a landless man.",
"Three on whom acknowledgment does not fall in its time: death, ignorance, carelessness.",
"Three oratories of Ireland: the oratory of Birr, the oratory of Clonenagh, the oratory of Leighlin.",
"Three ornaments of wisdom: abundance of knowledge, a number of precedents, to employ a good counsel.",
"Three perparations of a bad man’s house: strife before you, complaining to you, his hound taking hold of you.",
"Three places of Ireland to make you start: Tulach na n-Escop, Duma mBuirig.",
"Three pottages of guarantorship: wer-geld or a debtor’s . . . or non-possession (?)",
"Three preparations of a good man’s house: ale, a bath, a large fire.",
"Three prohibitions of food: to eat it without giving thanks, to eat it after a guest, to eat it before its proper time.",
"Three props of obstinacy: pledging oneself, contending, wrangling.",
"Three qualities that bespeak good fortune: self-importance, . . . , self-will.",
"Three qualities that bespeak misfortune: weariness, (premature) old age, reproachfulness.",
"Three ranks that ruin tribes in their falsehood: the falsehood of a king, of a historian, of a judge.",
"Three rejoicings followed by sorrow: a wooer’s, a thief’s, a tale-bearer’s.",
"Three rejoicings that are worse than sorrow: the joy of a man who has defrauded another, the joy of a man who has perjured himself, the joy of a man who has committed parricide.",
"Three renovators of the world: the womb of a woman, a cow’s udder, a smith’s moulding-block.",
"Three reverent sisters: usefulness, an easy bearing, firmness.",
"Three rocks to which lawful behaviour is tied: a monastery, a chieftain, the family.",
"Three rude ones of the world: a youngster mocking an old man, a healthy person mocking an invalid, a wise man mocking a fool.",
"Three ruins of a tribe: a lying chief, a false judge, a lustful priest.",
"Three sauces that spoil a sick-bed: . . ., honey, salt food.",
"Three services the worst that a man can serve: serving a bad woman, a bad lord, and a bad smith.",
"Three shouts of a good warrior’s house: the shout of distribution, the shout of sitting down, the shout of rising up.",
"Three signs of a fop: the track of his comb in his hair, the track of his teeth in his food, the track of his stick behind him.",
"Three signs of boorishness: strife, and contention, and mistaking a person for another (?)",
"Three signs of folly: contention, wrangling, attachment (to everybody).",
"Three signs of wisdom: patience, closeness, the gift of prophecy.",
"Three signs that . . . in a judge’s house: wisdom, information, intellect.",
"Three silences that are better than speech: silence during instruction, silence during music, silence during preaching.",
"Three sisters of good fortune: good breeding, liberality, mirth.",
"Three sisters of good repute: diligence, prudence, bountifulness.",
"Three sisters of ill repute: inertness, grudging, closefistedness.",
"Three slender things that best support the world: the slender stream of milk from the cow’s dug into the pail, the slender blade of green corn upon the ground, the slender thread over the hand of a skilled woman.",
"Three smiles that are worse than sorrow: the smile of the snow as it melts, the smile of your wife.",
"Three sons that do not share inheritance: a son begotten in a brake, the son of a slave, the son of a girl still wearing tresses.",
"Three sons whom chastity bears to wisdom: valour, generosity, laughter (filial piety?).",
"Three sons whom churlishness bears to impatience: trembling, niggardliness, vociferation.",
"Three sons whom folly bears to anger: frowning, . . . , mockery (?).",
"Three sons whom generosity bears to patience: . . . , blushing, shame.",
"Three sorrowful ones of an alehouse: the man who gives the feast, the man to whom it is given, the man who drinks without being satiated. ",
"Three sorrows that are better than joy: the heaviness of a herd feeding on mast, the heaviness of a ripe field, the heaviness of a wood under mast.",
"Three sounds of increase: the lowing of a cow in milk, the din of a smithy, the swish of a plough.",
"Three sparks that kindle love: a face, demeanour, speech.",
"Three speeches that are better than silence: inciting a king to battle, spreading knowledge (?).",
"Three steadinesses of good womanhood: keeping a steady tongue, a steady chastity, and a steady housewifery.",
"Three strayings of bad womanhood: letting her tongue, and . . . and her housewifery go astray.",
"Three tabus of a chief: an ale-house without story-telling, a troop without a herald, a great company without wolf-hounds.",
"Three that are incapable of special contracts: a son whose father is alive, a betrothed woman, the serf of a chief.",
"Three that are most difficult to talk to: a king about his booty, a viking in his hauberk, a boor who is under patronage.",
"Three that are not entitled to exemption: restoring a son, the tools of an artificer, hostageship.",
"Three that are not entitled to renunciation of authority: a son and his father, a wife and her husband, a serf and his lord.",
"Three that are not entitled to sick-maintenance: a man who absconds from his chief, from his family, from a poet.",
"Three that are worst in a house: boys, women, lewdness.",
"Three that neither swear nor are sworn: a woman, a son who does not support his father, a dumb person.",
"Three things betokening trouble: holding a plough-land in common, performing feats together, alliance in marriage.",
"Three things by which every angry person is known: an outburst of passion, trembling, growing pale.",
"Three things for which a friend is hated: trespassing, fecklessness.",
"Three things for which an enemy is loved: wealth, beauty, worth.",
"Three things hard to guarantee and to become a hostage and to make a contract for: to go security for constructing the fort of a king, an oratory, and a caldron. For it is hard for a man of a family to be given with (?) his fellow.",
"Three things that . . . salt-meat, butter, iron . . . .",
"Three things that are best for a chief: justice, peace, an army.",
"Three things that are best in a house: oxen, men, axes.",
"Three things that are undignified for everyone: driving one’s horse before one’s lord so as to soil his dress, going to speak to him without being summoned, staring in his face as he is eating his food.",
"Three things that are worst for a chief: sloth, treachery, evil counsel.",
"Three things that characterise every chaste person: steadiness, modesty, sobriety.",
"Three things that characterise every haughty person: pompousness, elegance, (display of) wealth.",
"Three things that characterise every patient person: repose, silence, blushing.",
"Three things that constitute a blacksmith: Nethin’s spit, the cooking-hearth of the Morrigan, the Dagda’s anvil.",
"Three things that constitute a buffoon: blowing out his cheek, blowing out his satchel, blowing out his belly.",
"Three things that constitute a carpenter: joining together without calculating (?), without warping (?); agility with the compass; a well-measured stroke.",
"Three things that constitute a comb-maker: racing a hound in contending for a bone; straightening a ram’s horn by his breath, without fire; chanting upon a dunghill so that all antlers and bones and horns that are below come to the top.",
"Three things that constitute a harper: a tune to make you cry, a tune to make you laugh, a tune to put you to sleep.",
"Three things that constitute a king: a contract with (other) kings, the feast of Tara, abundance during his reign.",
"Three things that constitute a physician: a complete cure, leaving no blemish behind, a painless examination.",
"Three things that constitute a poet: ‘knowledge that illumines’, ‘teinm laeda’, improvisation.",
"Three things that constitute an artificer: weaving chains, a mosaic ball, an edge upon a blade.",
"Three things that make a fool wise: learning, steadiness, docility.",
"Three things that make a wise man foolish: quarreling, anger, drunkenness.",
"Three things that ruin every chief: falsehood, overreaching, parricide.",
"Three things that ruin wisdom: ignorance, inaccurate knowledge, forgetfulness.",
"Three things that set waifs a-wandering: persecution, loss, poverty.",
"Three things that should be proclaimed: the flesh-fork of a caldron, a bill-hook without a rivet, a sledge-hammer without. . .",
"Three things that show a bad man: bitterness, hatred, cowardice.",
"Three things that show every good man: a special gift, valour, piety.",
"Three things that tell every humble person: poverty, homeliness, servility.",
"Three things which judgment demands: wisdom, penetration, knowledge.",
"Three things which justice demands: judgment, measure, conscience.",
"Three timid brothers: ‘hush!’ ‘stop!’ ‘listen!’",
"Three tokens of a blessed site: a bell, psalm-singing, a synod (of elders).",
"Three tokens of a cursed site: elder, a corncrake, nettles.",
"Three ugly things that hide fairness: a sweet-lowing cow without milk, a fine horse without speed, a fine person without substance.",
"Three unfortunate things for a householder: proposing to a bad woman, serving a bad chief, exchanging for bad land.",
"Three unfortunate things for a man: a scant drink of water, thirst in an ale-house, a narrow seat upon a field.",
"Three unfortunate things for the son of a peasant: marrying into the family of a franklin, attaching himself to the retinue of a king, consorting with theives.",
"Three unfortunate things of husbandry: a dirty field, leavings of the hurdle, a house full of sparks.",
"Three ungentlemanly boasts: I am on your track, I have trampled you, I have wet you with my dress.",
"Three ungentlemanly things: interrupting stories, a mischievous game, jesting so as to raise a blush.",
"Three usucaptions that are not entitled to a fine: fear, warning, asportation.",
"Three wages that labourers share: the wages of a caldron, the wages of a mill, the wages of a house.",
"Three waves without wisdom: hard pleading, judgment without knowledge, a talkative gathering.",
"Three wealths in barren places: a well in a mountain, fire out of a stone, wealth in the possession of a hard man.",
"Three welcomes of an alehouse: plenty and kindliness and art.",
"Three well-bred sisters: constancy, well-spokenness, kindliness.",
"Three who do not adjudicate though they are possessed of wisdom: a man who sues, a man who is being sued, a man who is bribed to give judgment.",
"Three whose spirits are highest: a young scholar after having read his psalms, a youngster who has put on man’s attire, a maiden who has been made a woman.",
"Three woman-days: Monday, Tuesday, Wednesday. If women go to men on those days, the men will love them better than they the men, and the women will survive the men.",
"Three women that are not entitled to a fine: a woman who does not care with whom she sleeps, a thievish woman, a sorceress.",
"Three wonders concerning the Táin Bó Cúailgne: the the ‘cuilmen’ came to Ireland in its stead; the dead relating it to the living, viz. Fergus mac Róig reciting it to Ninníne the poet in the time of Cormac mac Fáeláin; one year’s protection to him to whom it is recited.",
"Three wonders of Connaught: the grave of Eothaile on its strand.  It is as high as the strand.  When the sea rises, it is as high as the high tide.    The stone of the Dagda.  Though it be thrown into the sea, though it be put into a house under lock, . . . out of the well at which it is.    The two herons in Scattery island.  They let no other herons to them into the island, and the she-heron goes on the ocean westwards to hatch and returns thence with her young ones.  And coracles have not discovered the place of hatching.",
"Three wonders of Ireland: the grave of the dwarf.",
"Three worst smiles: the smile of a wave, the smile of a lewd woman, the grin of a dog ready to leap.",
"Three youthful sisters: desire, beauty, generosity.",
"Two brothers: prosperity and husbandry.",
"Two ominous cries of ill-luck: boasting of your first slaughter, and of your wife being with another man.",
"Two sisters: weariness and wretchedness.",
"What are the three wealths of fortunate people?  Not hard to tell.  A ready conveyance (?), ale without a habitation (?), a safeguard upon the road.",
"What is worst in a household? Sons of a bawd, frequent feasts, numerous alliances in marriage, abundance of mead and wine. They waste you and do not profit."
];

/**
 * The AlexaSkill prototype and helper functions
 */
var AlexaSkill = require('./AlexaSkill');

/**
 * SpaceGeek is a child of AlexaSkill.
 * To read more about inheritance in JavaScript, see the link below.
 *
 * @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Introduction_to_Object-Oriented_JavaScript#Inheritance
 */
var Fact = function () {
    AlexaSkill.call(this, APP_ID);
};

// Extend AlexaSkill
Fact.prototype = Object.create(AlexaSkill.prototype);
Fact.prototype.constructor = Fact;

Fact.prototype.eventHandlers.onSessionStarted = function (sessionStartedRequest, session) {
    //console.log("onSessionStarted requestId: " + sessionStartedRequest.requestId + ", sessionId: " + session.sessionId);
    // any initialization logic goes here
};

Fact.prototype.eventHandlers.onLaunch = function (launchRequest, session, response) {
    //console.log("onLaunch requestId: " + launchRequest.requestId + ", sessionId: " + session.sessionId);
    handleNewFactRequest(response);
};

/**
 * Overridden to show that a subclass can override this function to teardown session state.
 */
Fact.prototype.eventHandlers.onSessionEnded = function (sessionEndedRequest, session) {
    //console.log("onSessionEnded requestId: " + sessionEndedRequest.requestId + ", sessionId: " + session.sessionId);
    // any cleanup logic goes here
};

Fact.prototype.intentHandlers = {
    "GetNewFactIntent": function (intent, session, response) {
        handleNewFactRequest(response);
    },

    "AMAZON.HelpIntent": function (intent, session, response) {
        response.ask("You can say tell me a triad, or, you can say exit... What can I help you with?", "What can I help you with?");
    },

    "AMAZON.StopIntent": function (intent, session, response) {
        var speechOutput = "Goodbye";
        response.tell(speechOutput);
    },

    "AMAZON.CancelIntent": function (intent, session, response) {
        var speechOutput = "Goodbye";
        response.tell(speechOutput);
    }
};

/**
 * Gets a random new fact from the list and returns to the user.
 */
function handleNewFactRequest(response) {
    // Get a random space fact from the space facts list
    var factIndex = Math.floor(Math.random() * FACTS.length);
    var randomFact = FACTS[factIndex];

    // Create speech output
    var speechOutput = "Here's your triad: " + randomFact;
    var cardTitle = "Your Triad";
    response.tellWithCard(speechOutput, cardTitle, speechOutput);
}

// Create the handler that responds to the Alexa Request.
exports.handler = function (event, context) {
    // Create an instance of the SpaceGeek skill.
    var fact = new Fact();
    fact.execute(event, context);
};

